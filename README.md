# Mission 21
# FAQ chatbot for WTEF
Group Number: 21
Team Members: Hima Bindu, Riddhi More, Shreeji

Overview: Our project aims to build a chatbot for Women Techmakers Engineering Fellows(WTEF) website.
We have decided to make a FAQ chatbot for WTEF website which will answer all the people who reach with some general questions. For now, we will make a static chatbot and will train it with many possible questions that a user might ask and with many phrases that result in the same answer.

Targeted audience:
Anyone who is interested in the WTEF program and comes to the WTEF’s site to resolve their queries.


Tools:

Google Dialogflow :  Over 100 Intents and 700 Questions, rich content including images, gifs and lists.
Janis  API : Used for Human Handoff.
Botcopy : Provides the UI and links Dialogflow to Janis




